# ABADONED

This service is no longer maintained and developed.

# BAPH

Überwachungs- und Kommunikationsöglichkeit für Babys und pflegebedürftige Menschen. Um die Anwendung zu nutzen ist nur ein Browser notwendig, weshalb fast alle mobilen Geräte, Laptops und PCs verwendet werden können.

**Demo Website:** baph.sz-ybbs.ac.at

## Funktionen

* verschlüsselte P2P Audio- und Video-Streams
* eigene Einstellungen

### Unterstütze Browser

* Google Chrome und chromium-basierte Browser
* manche Versionen von Firefox
* Opera

## TODO

* Audio visualisieren
* Email validieren
* Sound beim abgebrochene Verbindungen abspielen
* besseres Management-System für die Website

## In Betracht

* Eslint oder Typescript mit Tslint verwenden
* Neues/verbessertes Design
