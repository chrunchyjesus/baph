/*global opr, safari */
(function() {
  // Opera 8.0+
  var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
  var isOperaMini = Object.prototype.toString.call(window.operamini) === "[object OperaMini]";

  // Firefox 1.0+
  var isFirefox = typeof InstallTrigger !== 'undefined';

  // Safari 3.0+ "[object HTMLElementConstructor]"
  //   var isSafari = /constructor/i.test(window.HTMLElement) || (function(p) {
  //     return p.toString() === "[object SafariRemoteNotification]";
  //   })(!window.safari || safari.pushNotification);

  // Internet Explorer 6-11
  //   var isIE = /*@cc_on!@*/ false || !!document.documentMode;

  // Edge 20+
  //   var isEdge = !isIE && !!window.StyleMedia;

  // Chrome 1+
  var isChrome = (!!window.chrome && !!window.chrome.webstore)||isChromef();
  //alert(!!window.chrome + ' ' + !!window.chrome.webstore);
  // Blink engine detection
  // var isBlink = (isChrome || isOpera) && !!window.CSS;

  if (!(isOpera || isFirefox || isChrome) || isOperaMini) {
    // not supported Browser
    console.error('Your browser is not supported.');
    alert('We currently only support the latest Firefox, Chrome and Opera browser. Opera Mini (mobile version of opera) is excluded.');
  }
  //   console.log("Is Opera: ", isOpera);
  //   console.log("Is Firefox: ", isFirefox);
  //   console.log("Is Safari: ", isSafari);
  //   console.log("Is IE: ", isIE);
  //   console.log("Is Edge: ", isEdge);
  //   console.log("Is Chrome: ", isChrome);
  //   console.log("Is Blink: ", isBlink);
  function isChromef() {
  var isChromium = window.chrome,
    winNav = window.navigator,
    vendorName = winNav.vendor,
    isOpera = winNav.userAgent.indexOf("OPR") > -1,
    isIEedge = winNav.userAgent.indexOf("Edge") > -1,
    isIOSChrome = winNav.userAgent.match("CriOS");

  if(isIOSChrome){
    return true;
  } else if(isChromium !== null && isChromium !== undefined && vendorName === "Google Inc." && isOpera === false && isIEedge === false) {
    return true;
  } else {
    return false;
  }
}
})();
