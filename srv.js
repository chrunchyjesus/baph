//@ts-check
//jshint node:true, esversion:6, browser:false, -W034
// 'use strict';

var https = require('https');
var fs = require('fs');

var settings = {
  maxclients: 2,
  port: 44000,
  options: {
    key: fs.readFileSync('cert/key.pem'),
    cert: fs.readFileSync('cert/cert.pem')
  }
};

// var io = require('socket.io')(settings.port);
var app = https.createServer(settings.options);
var io = require('socket.io').listen(app); //socket.io server listens to https connections
app.listen(settings.port, "192.168.100.210");

var rooms = io.sockets.adapter.rooms;


io.sockets.on('connection', function(socket) {
  // console.log('A new client connected: ' + socket.id);
  socket.emit('init', {
    id: socket.id
  });

  /*socket.on('create', function(obj) {
    // console.log('The client ' + socket.id + ' request to create the room ' + obj.roomName + '.');
    if (!rooms[obj.roomName]) { // create the room
      rooms[obj.roomName] = obj;
      rooms[obj.roomName].clients = [];
      rooms[obj.roomName].clients.push(socket);

      socket.join(obj.roomName);
      // console.log('The client ' + socket.id + ' created the room ' + obj.roomName + '.');
      socket.emit('created', {
        roomName: obj.roomName
      });
    } else { // room already exists
      socket.emit('log', 'The room already exists.');
    }
  });
  socket.on('join', function(obj) {
    // console.log('The client ' + socket.id + ' request to join the room ' + obj.roomName + '.');
    if (rooms[obj.roomName]) { // if the room exists
      if (rooms[obj.roomName].pwd === obj.pwd) { // join the room
        if (rooms[obj.roomName].clients.length < settings.maxclients) {
          socket.join(obj.roomName);
          rooms[obj.roomName].clients.push(socket);
          // console.log('The client ' + socket.id + ' joined the room ' + obj.roomName + '.');
          socket.emit('joined', {
            roomName: obj.roomName
          });
        } else {
          socket.emit('log', 'The room is full.');
        }
      } else { // wrong password but room exists
        socket.emit('log', 'The rooms exists but your password is wrong.');
      }
    } else { // room does not exist
      socket.emit('room does not exist', obj);
    }
  });*/
  socket.on('create or join', function(obj) {
    if (!rooms[obj.roomName]) { // create the room
      socket.join(obj.roomName);
      Object.defineProperty(rooms[obj.roomName],
        "pwd", {
          value: obj.pwd,
          writable: false,
          enumerable: false,
          configurable: false
        }
      );
      // rooms[obj.roomName] = obj;
      // rooms[obj.roomName].clients = [];
      // rooms[obj.roomName].clients.push(socket);

      // console.log('The client ' + socket.id + ' created the room ' + obj.roomName + '.');
      socket.emit('created', {
        roomName: obj.roomName
      });
    } else if (rooms[obj.roomName]) { // if the room exists
      if (rooms[obj.roomName].pwd === obj.pwd) { // join the room
        if (rooms[obj.roomName].length < settings.maxclients) {
          socket.join(obj.roomName);
          // rooms[obj.roomName].clients.push(socket);
          // console.log('The client ' + socket.id + ' joined the room ' + obj.roomName + '.');
          socket.emit('joined', {
            roomName: obj.roomName
          });
        } else {
          socket.emit('log', 'The room is full.');
        }
      } else { // wrong password but room exists
        socket.emit('log', 'The rooms exists but your password is wrong.');
      }
    } else { // room does not exist
      socket.emit('log', 'Something went wrong at processing room name. Please try again later.');
    }
  });
  socket.on('leave', function(obj) {
    // var index = rooms[obj.room].clients.indexOf(socket);
    // io.emit('log', 'Index: '+index+'; Room: '+obj.room);
    // if (index > -1) {
      socket.broadcast.to(obj.room).emit('log', 'Someone disconnected');
      socket.leave(obj.room);
    //   if (rooms[obj.room].clients.length === 1) {
    //     rooms[obj.room] = false;
    //     delete rooms[obj.room];
    //     // console.log('Deleted the room ' + obj.room + '.');
    //   } else {
    //     rooms[obj.room].clients.splice(index, 1);
    //   }
    // }
  });
  //broadcasts
  socket.on('webrtc', function(obj) {
    // check if socket is in the given room
    if (io.sockets.adapter.sids[socket.id][obj.room] === true) {
      socket.broadcast.to(obj.room).emit('start chat');
    }
  });
  socket.on('message', function(obj) {
    if (io.sockets.adapter.sids[socket.id][obj[0].room] === true) {
      socket.broadcast.to(obj[0].room).emit('msg', obj[1]);
      // console.log('Client ' + socket.id + ' is sending a ' +
      //   ((obj[0].tp) ? obj[0].tp : '') +
      //   ' broadcast.');
    }
  });

  socket.on('disconnecting', function () {
    for (var room in socket.rooms) {
      if (socket.rooms.hasOwnProperty(room)) {
        socket.broadcast.to(room).emit('log', 'Someone disconnected');
      }
    }
  });
  socket.on('disconnect', function () {
    // socket.rooms
    socket.leaveAll();
    // console.log('Client ' + socket.id + ' has disconnected.');
    // for (var room in rooms) {
    //   if (rooms.hasOwnProperty(room)) {
    //     var index = rooms[room].clients.indexOf(socket);
    //     if (index > -1) {
    //       socket.broadcast.to(room).emit('log', 'Someone disconnected');
    //       if (rooms[room].clients.length === 1) {
    //         rooms[room] = false;
    //         delete rooms[room];
    //         // console.log('Deleted the room ' + room + '.');
    //       } else {
    //         rooms[room].clients.splice(index, 1);
    //       }
    //     }
    //   }
    // }
  });
});
